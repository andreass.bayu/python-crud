from os import system
from sys import platform

def clear():
	if platform == "linux" or platform == "linux2" and platform == "darwin":
		system('clear')
	else:
		system('cls')

dataMahasiswa = [
    {
        "nama" : "Budi",
        "alamat" : "Depok"
    },
    {
        "nama" : "Jono",
        "alamat" : "Jakarta"
    },
    {
        "nama" : "Ilham",
        "alamat" : "Bogor"
    }
]

def menu():
    print("=== MENU ===")
    print("1. Lihat Data")
    pilihan = int(input("Pilih nomor (1) : "))
    return pilihan

def tryAgain():
    pilihan = input("Kembali ke menu awal? (Y/T) : ")
    if pilihan == "Y" or pilihan == "y":
        start()
    else:
        print("Keluar dari program!!")
        print("Good Bye")
        exit()

def start():
    clear()
    choosen = menu()
    if choosen == 1:
        getData()
    else:
        clear()
        print("Salah pilih nomor")
        tryAgain()

def repeatGetdata():
    ulang = input("Cari nama lagi? (Y/T) : ")
    if ulang == "Y" or ulang == "y":
        getData()
    else:
        tryAgain()

def getData():
    clear()
    data = {}
    pilihan = input("Masukkan nama yang mau di cari : ")
    for item in dataMahasiswa:
        if item['nama'] == pilihan:
            data.update(item)
            break
    else:
       print("Data tidak ditemukan")
       repeatGetdata()
    print("=== Hasil Pencarian ===")
    print("Nama = ", data['nama'])
    print("Alamat = ", data['alamat'])    
    repeatGetdata()
          

start()
